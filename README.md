# :rotating_light: strobelight :rotating_light:

[![conda](https://img.shields.io/conda/dn/ostrokach-forge/strobelight.svg)](https://anaconda.org/ostrokach-forge/strobelight/)
[![docs](https://img.shields.io/badge/docs-v0.1.0-blue.svg)](https://ostrokach.gitlab.io/strobelight/v0.1.0/)
[![pipeline status](https://gitlab.com/ostrokach/strobelight/badges/v0.1.0/pipeline.svg)](https://gitlab.com/ostrokach/strobelight/commits/v0.1.0/)
[![coverage report](https://gitlab.com/ostrokach/strobelight/badges/v0.1.0/coverage.svg)](https://ostrokach.gitlab.io/strobelight/v0.1.0/htmlcov/)


An opinionated library for working with protein structures using the pandas DataFrame API.

## Features

- A flexible way of extracting and combinding different models, chains and / or residues, while trying to maintain pertinent information, including the list of covalent bonds.
- Fast reading and writing of PDB and mmCIF file formats.
- Support for generating protein bioassemblies using information in PDB headers or mmCIF fields.
- Includes a range of common protein analysis routines.
- Seamless interoperability with other packages for protein analysis and visualization, including `BioPython`, `MDTraj`, `nglview`, and `3dmol`.

## Examples

Extract the first chain from a structure.

```python
import strobelight as sl

s = sl.fetch("4dkl.cif")
s = sl.Structure(s.df[s.df["chain_idx"] == 0])
```

Select residues `10 to 20` and `121 to 140` from the first chain.

```python
s = sl.Structure(
    s.df[
      (s.df["chain_idx"] == 0)
      & (s.df["residue_idx"] >= 10)
      & (s.df["residue_idx"] < 20)
    ],
    s.df[
      (s.df["chain_idx"] == 0)
      & (s.df["residue_idx"] >= 121)
      & (s.df["residue_idx"] < 140)
    ],
    level="residue",
)
```

Load a specific bioassembly.

```python
s = sl.fetch("4dkl.cif", bioassembly_id=1)
```

## Comparison to other softare

### BioPython

### BioPandas

### MDTraj

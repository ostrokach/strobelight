import pytest

import strobelight


@pytest.mark.parametrize("attribute", ["__version__"])
def test_attribute(attribute):
    assert getattr(strobelight, attribute)


def test_main():
    import strobelight

    assert strobelight

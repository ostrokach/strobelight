from enum import Enum
from pathlib import Path
from typing import TYPE_CHECKING, List, Optional, Tuple, Union

import numpy as np
import pandas as pd

if TYPE_CHECKING:
    try:
        from Bio import PDB
    except ImportError:
        pass
    try:
        import mdtraj
    except ImportError:
        pass
    try:
        import nglview
    except ImportError:
        pass
    try:
        import py3Dmol
    except ImportError:
        pass


class StructureLevel(Enum):
    STRUCTURE = 1
    structure = 1

    MODEL = 2
    model = 2

    CHAIN = 3
    chain = 3

    RESIDUE = 4
    residue = 4

    ATOM = 5
    atom = 5


class Structure:

    columns: List[str] = [
        # Structure
        "structure_id",
        # Model
        "model_id",
        "model_idx",
        # Chain
        "chain_id",
        "chain_idx",
        # Residue
        "residue_id",  # resSeq + insertion
        "residue_name",  # resName
        "residue_idx",
        # Atom
        "atom_id",  # serial
        "atom_name",
        "atom_idx",
        "x_coord",
        "y_coord",
        "z_coord",
        "occupancy",
        "bfactor",
        "segment_id",  # segmentID
        "element",  # element_symbol
        "charge",
        "bonded_atom_idxs",
    ]

    def __init__(self, *dfs: pd.DataFrame, level: Union[str, StructureLevel] = "model") -> None:
        """Create a new :class:`Structure` instance.

        Args:
            dfs: One or more DataFrames from which to construct the structure.
            level: If multiple DataFrames are given, the level at which those dataframes
                should be combined. The possible options are:
                - ``atom`` - The DataFrames will be treated as collections of different atoms
                    corresponding to the same residue.
                - ``residue`` - The DataFrames will be treated as collections of different residues
                    correspondijng to the same chain and model.
                - ``chain`` - The DataFrames will be treated as different chains corresponding
                    to the same model.
                - ``model`` - The DataFrames will be treated as different models corresponding
                    to the same structure.
        """

        def reindex(df: pd.DataFrame, column: str, start_idx: int = 0) -> Tuple[pd.DataFrame, int]:
            assert df[column].is_monotonic_increasing()
            old_to_new = {
                residue_idx: start_idx + i for i, residue_idx in enumerate(df[column].unique())
            }
            df[column] = df[column].map(old_to_new)
            return df, start_idx + len(old_to_new)

        atom_idx = 0
        residue_idx = 0
        chain_idx = 0
        model_idx = 0
        dfs_corrected = []
        for df in dfs:
            df = df[self.columns].copy()

            df, atom_idx = reindex(df, column="atom_idx", start_idx=atom_idx)

            if level in [StructureLevel.ATOM]:
                df["residue_idx"] = residue_idx
            else:
                df, residue_idx = reindex(df, column="residue_idx", start_idx=residue_idx)

            if level in [StructureLevel.ATOM, StructureLevel.RESIDUE]:
                df["chain_idx"] = chain_idx
            else:
                df, chain_idx = reindex(df, column="chain_idx", start_idx=chain_idx)

            if level in [StructureLevel.ATOM, StructureLevel.RESIDUE, StructureLevel.CHAIN]:
                df["model_idx"] = model_idx
            else:
                df, model_idx = reindex(df, column="model_idx", start_idx=model_idx)
            dfs_corrected.append(df)
        df = pd.concat(dfs_corrected, ignore_index=True)
        assert (df.index.values == np.arange(len(df))).all()
        assert (df["atom_idx"].values == np.arange(len(df))).all()
        self._df = df

    @property
    def df(self) -> pd.DataFrame:
        return self._df

    @classmethod
    def load(cls, filename: Union[str, Path], bioassembly_id: Optional[int] = None) -> "Structure":
        filename = Path(filename)
        if ".pdb" in filename.suffixes:
            return cls.load_pdb(filename)
        elif ".cif" in filename.suffixes or ".mmcif" in filename.suffixes:
            return cls.load_mmcif(filename)
        else:
            raise ValueError(f"Unrecognized file extension for file: '{filename}'.")

    @classmethod
    def load_pdb(
        cls, filename: Union[str, Path], bioassembly_id: Optional[int] = None
    ) -> "Structure":
        raise NotImplementedError

    @classmethod
    def load_mmcif(
        cls, filename: Union[str, Path], bioassembly_id: Optional[int] = None
    ) -> "Structure":
        raise NotImplementedError

    def save(self, filename: Union[str, Path]) -> None:
        filename = Path(filename)
        if ".pdb" in filename.suffixes:
            return self.save_pdb(filename)
        elif ".cif" in filename.suffixes or ".mmcif" in filename.suffixes:
            return self.save_mmcif(filename)
        else:
            raise ValueError(f"Unrecognized file extension for file: '{filename}'.")

    def save_pdb(self, filename: Union[str, Path]) -> None:
        from strobelight.io import save_pdb

        return save_pdb(filename)

    def save_mmcif(self, filename: Union[str, Path]) -> None:
        from strobelight.io import save_mmcif

        return save_mmcif(filename)

    def to_biopython(self) -> "PDB.Structure":
        from strobelight.interop.biopython import df_to_biopython

        s = df_to_biopython(self.df)
        return s

    @classmethod
    def from_biopython(cls, s: "PDB.Structure") -> "Structure":
        from strobelight.interop.biopython import biopython_to_df

        df = biopython_to_df(s)
        return Structure(df)

    def to_mdtraj(self) -> "mdtraj.Trajectory":
        from strobelight.interop.mdtraj import df_to_mdtraj

        s = df_to_mdtraj(self.df)
        return s

    @classmethod
    def from_mdtraj(cls, traj: "mdtraj.Trajectory") -> "Structure":
        from strobelight.interop.mdtraj import mdtraj_to_df

        df = mdtraj_to_df(traj)
        return Structure(df)

    def to_nglview(self) -> "nglview.View":
        from strobelight.interop.nglview import df_to_nglview

        s = df_to_nglview(self.df)
        return s

    def to_3dmol(self) -> "py3Dmol.view":
        from strobelight.interop.py3dmol import df_to_3dmol

        s = df_to_3dmol(self.df)
        return s

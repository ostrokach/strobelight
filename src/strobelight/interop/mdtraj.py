import mdtraj
import pandas as pd


def df_to_mdtraj(df: pd.DataFrame) -> mdtraj.Trajectory:
    raise NotImplementedError


def mdtraj_to_df(traj: mdtraj.Trajectory) -> pd.DataFrame:
    raise NotImplementedError

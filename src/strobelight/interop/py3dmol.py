import pandas as pd
import py3dmol


def df_to_3dmol(df: pd.DataFrame) -> py3dmol.view:
    assert py3dmol
    raise NotImplementedError

import nglview
import pandas as pd


def df_to_nglview(df: pd.DataFrame) -> nglview.View:
    assert nglview
    raise NotImplementedError
